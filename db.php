<?php

//Code by Shawn Drake - June 2009


function db_connection() {
	
	require('survey_config.php');
	
	switch ($dbtype) {
		case 'mysql':
			$dbconnect = mysql_connect($dbhost, $dbuser, $dbpass) or die(mysql_error());
			$dbselect = mysql_select_db($dbname) or die(mysql_error());
		break;
		case 'mssql':
			$dbconnect = mssql_connect($dbhost, $dbuser, $dbpass);
			$dbselect = mssql_select_db($dbname);
		break;
	}
}

function query_function($query) {

	require('survey_config.php');

	switch ($dbtype) {
		case 'mysql':
			$result = mysql_query($query) or die(mysql_error());
		break;
		case 'mssql':
			$result = mssql_query($query);
		break;
	}
		
	return $result;
}

function count_results($query_result) {
	
	require('survey_config.php');

	switch ($dbtype) {
		case 'mysql':
			$record_count = mysql_num_rows($query_result);
		break;
		case 'mssql':
			$record_count = mssql_num_rows($query_result);
		break;
	}
		
	return $record_count;
}

function fetch_assc($query_result) {
	
	require('survey_config.php');

	switch ($dbtype) {
		case 'mysql':
			$fetch = mysql_fetch_assoc($query_result);
		break;
		case 'mssql':
			$fetch = mssql_fetch_assoc($query_result);
		break;
	}
		
	return $fetch;
}

?>