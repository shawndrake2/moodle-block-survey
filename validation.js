// JavaScript Document
/* <![CDATA[ */

//Shawn Drake (June 2009)
//Form Check JavaScript
<!--HIDE FROM INCOMPATIBLE BROWSERS
//STOP HIDING FROM INCOMPATIBLE BROWSERS -->

function validate_JS()
{
	document.getElementById('container').style.display = 'block';
	var olddiv = document.getElementById('no_validate');
	document.getElementById('validate').removeChild(olddiv);
	document.bgColor = '#cccccc';
	setFormFocus('t_s_month');
}

function setFormFocus(element)
{
	document.forms[0].element.focus();
}

function checkMonth(month_entered, year_entered)
 {
     return 32 - new Date(year_entered, (month_entered - 1), 32).getDate();
 }

function surveyMessage(var_name)
{
	switch (true)
	{
		case (var_name.indexOf("t_s")!=-1):
			var message = 0;
			break;
		case (var_name.indexOf("t_e")!=-1):
			var message = 1;
			break;
		case (var_name.indexOf("s_s")!=-1):
			var message = 2;
			break;
		case (var_name.indexOf("s_e")!=-1):
			var message = 3;
			break;
	}
	
	return message;

}

function changeContent(row_num){

	var x=document.getElementById('input_dates').rows
    var y=x[row_num].cells
    y[2].innerHTML="<p style='color:#F00;'>Required Field</p>";
}

function removeError(row_num){
	
	var x=document.getElementById('input_dates').rows
    var y=x[row_num].cells
    y[2].innerHTML="<p style='color:#F00;'>&nbsp;</p>";
}

function confirmSubmit(month_entered, day_entered, year_entered, name)
{
	var message = surveyMessage(name);
	var days = checkMonth(month_entered, year_entered);
	
	if (day_entered == "")
	{
		changeContent(message);
		return false;
	} else {
		removeError(message);
		
		if ((day_entered < 1) || (day_entered > days))
		{
			window.alert("Date entered must be between 1 and " + days + ".");
			return false;
		}	
		
		switch (name) 
		{
			case 't_s_day':
				document.forms[0].t_e_day.disabled = false;
				break;
			case 't_e_day':
				document.forms[0].s_s_day.disabled = false;
				break;
			case 's_s_day':
				document.forms[0].s_e_day.disabled = false;
				break;
			case 's_e_day':
				document.forms[0].SubmitForm.disabled = false;
				break;
		}
	}
	
	return true;
}

/*]]*/
