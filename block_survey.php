<?php

//Code by Shawn Drake - June 2009
//
//Updated December 2010

class block_survey extends block_base {
	
	function init() {
    	$this->title   = 'End of Term Survey';   
    	$this->version = 2009052600;
  	}
	
	function applicable_formats() {
 		return array('all' => true);
	}
	
	function hide_header() {
	  return true;
	}
	
	function preferred_width() {
	  return 210;
	}

	function get_content() {
    	
		if ($this->content !== NULL) {
    		return $this->content;
    	}
		
		require_once ('db.php');
		
		$this->content = new stdClass;
		
		//intitialize today's date
		$today = mktime(0, 0, 0, date('m'), date('d'), date('Y')); 

		//create table mdl_term_info if it doesn't already exist
		$query = 'CREATE TABLE IF NOT EXISTS mdl_term_info (termid BIGINT(6), term_startdate BIGINT(15), term_closedate BIGINT(15), startdate BIGINT(15), closedate BIGINT(15), link TEXT(256))';
		$result = query_function($query);

		//query the term information
		$query = 'SELECT * FROM mdl_term_info WHERE startdate <= "'.$today.'" AND closedate >= "'.$today.'"';
		$result = query_function($query);
		$term_info = fetch_assc($result);
		
		//determine if survey should be active
		if (count($term_info) == 1) {
			
			$survey_open = 0;
			$term = ' ';
			
		} else {
			
			$survey_open = 1;
			
			//get the link
			$query = 'SELECT link FROM mdl_term_info WHERE link <> "" AND startdate <= "'.$today.'"';
			$result = query_function($query);
			$link_array = '';
			while ($link_results = fetch_assc($result)) {
				$link_array = $link_array.':::::'.$link_results['link'];
			}
			$link_array = explode(':::::',$link_array);
			$count = count($link_array) - 1;
			$survey_link = $link_array[$count];
			
			//validate link
			$pos = strpos($survey_link, 'http://');
			if ($pos === false) {
				$survey_link = 'http://'.$survey_link; 
			}
			
			$pos = strrpos($survey_link, '?id=');	
			$str_len = strlen($survey_link);
			if (($pos === false)) { 
				$survey_link = $survey_link.'?id='; 
			}
			
			//determine the closing date and the term
			$term = $term_info['termid'];
			$close_date = date('F\ jS\, Y', $term_info['closedate']);

		}
		
		//if not the correct dates, block won't show at all to students
		if ($survey_open == 1) {
			
			global $COURSE, $USER, $CFG;

			//retrieve our course information to be passed to survey
			$full = $USER->username;
			
			//find valid parent category number for ECPI 
			$parent_results = query_function('SELECT id FROM mdl_course_categories WHERE name = "ECPI Term Shells"'); 
			$row = fetch_assc($parent_results);
			$ecpi_term = $row['id'];
		
			//find all categories that fall under valid parent
			if ($term !== ' ') {
				$category_results = query_function('SELECT id FROM mdl_course_categories WHERE name = "'.$term.'" AND (parent = "'.$ecpi_term.'")');
				$counter = 0;
				while ($row = fetch_assc($category_results)) {
					$results[$counter] = $row['id'];
					$counter ++;
				}
			} else {
				$results = ' ';
			}

			//if current category and date are valid, show block, otherwise show nothing
			if (in_array($COURSE->category, $results)) {
				$this->content->text = '<img src="http://ecpicollege.com/images/survey/seotclogolarge.png" alt="survey.png" align="left" height="40" width="40"><p class="LargeTitles" style=" position: relative; margin-left:45px; margin-top:11px;">End of Term Survey</p><div style="margin: 4px;"><a href="'.$survey_link.$full.'" target="_blank">Click here</a> to take the End of Term Survey.<br /><br />This survey will be available until <strong>'.$close_date.' at 11:00pm EST</strong>.</div>';
			} else {
				$this->content->text = '';
			}	
		
		} else {
			$this->content->text = '';
		}
		
	    $this->content->footer = '';
 
		return $this->content;
	  			
	}
		
} 
?>