<!--- Code by Shawn Drake June 2009 -->

<script src="validation.js" language="javascript" type="text/javascript"></script>

<?php

require_once ('survey_config.php');
require_once ('survey_functions.php');
require_once ('db.php');

	if ((isset($_POST['t_s_month'])) && (isset($_POST['t_s_day'])) && (isset($_POST['t_e_month'])) && (isset($_POST['t_e_day'])) && (isset($_POST['s_s_month'])) && (isset($_POST['s_s_day'])) && (isset($_POST['s_e_month'])) && (isset($_POST['s_e_day']))) {
		$validation = validate_entries($_POST['t_s_month'], $_POST['t_s_day'], $_POST['t_s_year'], $_POST['t_e_month'], $_POST['t_e_day'], $_POST['t_e_year'], $_POST['s_s_month'], $_POST['s_s_day'], $_POST['s_s_year'], $_POST['s_e_month'], $_POST['s_e_day'], $_POST['s_e_year']);
	} else {
		$validation = 0;
	}
		
	$feedback = ' ';
		
	echo '<style type="text/css">
		body
		{
			font-family : Arial, Helvetica, sans-serif;
			font-size : 10.0pt;
			color: Black;
			width: 98%;
		}
		
		ul 
		{
			color: Black;
			font-family: Arial,Helvetica,sans-serif;
			font-size: 10.0pt;
		}
		
		ol
		{
			color: Black;
			font-family: Arial,Helvetica,sans-serif;
			font-size: 10.0pt;
		}
		
		li
		{
			color: Black;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 10pt;
		}
		
		a
		{
			color: #4B77A4;
		}
		
		a:visited
		{
			color:  #4B77A4;
		}
		a:hover
		{
			color : #999999;
		}
		
		
		p 	{
			color : Black;
			font-family : Arial, Helvetica, sans-serif;
			font-size : 10.0pt;
			text-align : left;
		}
		
		.container 
		{
			background-image: url(images/logo_watermark.png); 
			background-size: 200px 200px;
			background-color: #ffffff;
			background-position: bottom left;
			background-repeat:no-repeat;
			text-align: center;
			width: 1000px;
			margin: auto;
			border: outset;
		}
		
		.Text
		{
			color : Black;
			font-family : Arial, Helvetica, sans-serif;
			font-size : 10pt;
			font-style: normal;
			font-weight: normal;
		}
		
		.Titles
		{
			font-family: Arial, Helvetica, sans-serif;
			font-size : 10pt;
			color : #104B86;
			text-decoration : none;
			font-weight: bold;
		}
		
		.important {
			color: #F00;
			font-weight: bold;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 12pt;
		}
		
		.feedback {
			margin-left: 90px;
			margin-top: 90px;
			margin-bottom: -90px;
		}
		
		.error {
			width = 300px;
			height = 300px;
			background-color:#FFF;
			border-color:#FF0;
			text-align:center;
			margin:auto;
		}
		
		</style>
		
		<html>
		<head><title>End of Term Survey Dates</title></head>
		<body onload="validate_JS();">
		<div id="validate" style="text-align:center;">
		<div class="container" id="container" style="display: none;">
		<table height="100" width="100%"><tr valign="top"><td><div style="margin-left: 90px;">';
		
	//connect to the database
	db_connection();
	
	//create table mdl_term_info if it doesn't already exist
	$query = 'CREATE TABLE IF NOT EXISTS mdl_term_info (termid BIGINT(6), term_startdate BIGINT(15), term_closedate BIGINT(15), startdate BIGINT(15), closedate BIGINT(15), link TEXT(256))';
	$result = query_function($query);
			
	if ($validation == 1){
			
			if (strlen($_POST['t_s_month']) == 1) {
				$termid = $_POST['t_s_year'].'0'.$_POST['t_s_month'];
			} else {
				$termid = $_POST['t_s_year'].$_POST['t_s_month'];
			}
			$t_s_month = $_POST['t_s_month'];
			$t_s_day = $_POST['t_s_day'];
			$t_s_year = $_POST['t_s_year'];
			$term_startdate = mktime(0, 0, 0, $t_s_month, $t_s_day, $t_s_year);
			$t_e_month = $_POST['t_e_month'];
			$t_e_day = $_POST['t_e_day'];
			$t_e_year = $_POST['t_e_year'];
			$term_closedate = mktime(0, 0, 0, $t_e_month, $t_e_day, $t_e_year);
			$s_s_month = $_POST['s_s_month'];
			if (strlen($_POST['s_s_day']) == 1) {
				$s_s_day = '0'.$_POST['s_s_day'];
			}else {
				$s_s_day = $_POST['s_s_day'];
			}
			$s_s_year = $_POST['s_s_year'];
			$startdate = mktime(0, 0, 0, $s_s_month, $s_s_day, $s_s_year);
			$s_e_month = $_POST['s_e_month'];
			if (strlen($_POST['s_e_day']) == 1) {
				$s_e_day = '0'.$_POST['s_e_day'];
			}else {
				$s_e_day = $_POST['s_e_day'];
			}
			$s_e_year = $_POST['s_e_year'];
			$closedate = mktime(0, 0, 0, $s_e_month, $s_e_day, $s_e_year);
			$link = $_POST['link'];		
			
			$query = 'SELECT termid FROM mdl_term_info WHERE termid = "'.$termid.'"';
			$result = query_function($query);
			if(count_results($result) > 0) {
				$exists = 1;
			} else {
				$exists = 0;
			}
	
			if ($exists == 1) {
				$query = 'UPDATE mdl_term_info SET term_startdate="'.$term_startdate.'", term_closedate="'.$term_closedate.'", startdate="'.$startdate.'", closedate="'.$closedate.'"';
				if ($link !== '') {
					$query = $query.', link="'.$link.'"';
				}
				$query = $query.' WHERE termid="'.$termid.'"';
			} else {
				$query = 'INSERT INTO mdl_term_info (termid, startdate, closedate, term_startdate, term_closedate, link) VALUES ("'.$termid.'", "'.$startdate.'", "'.$closedate.'", "'.$term_startdate.'", "'.$term_closedate.'", "'.$link.'")';
			}
			$result = query_function($query);
			$feedback = '<p class="important"><strong>Records ';
			if ($exists == 1) {
				$feedback = $feedback.'updated';
			} else {
				$feedback = $feedback.'inserted';
			}
			$feedback = $feedback.' successfully!!</strong></p><br><br><br><br>';
		
	} 
			
	//initialize variables
	$year = date('y');
	
	echo '<p class="Titles">Enter information below for current term:</p>
		  <form action="survey_info.php" method="POST">
		  <table class="Text" id="input_dates">
		  <tr>
		  <td>Term Begins:</td>
		  <td><p><select name="t_s_month">';
		  
	populate_month();
	
	echo	  '</select>
			  &nbsp;
			  <input type="text" maxlength="2" size="1" name="t_s_day" onblur="confirmSubmit(document.forms[0].t_s_month.value, document.forms[0].t_s_day.value, document.forms[0].t_s_year.value, document.forms[0].t_s_day.name);">
			  , <select name="t_s_year">';
	populate_year();
	
	echo	'</select></td><td width="100px">&nbsp;</td>
		  </tr>
		  <tr>
		  <td>Term Ends:</td>
		  <td><p><select name="t_e_month">';
		  
	populate_month();
	
	echo	  '</select>
			  &nbsp;
			  <input type="text" maxlength="2" size="1" name="t_e_day" onblur="confirmSubmit(document.forms[0].t_e_month.value, document.forms[0].t_e_day.value, document.forms[0].t_e_year.value, document.forms[0].t_e_day.name);" disabled="true">
			  , <select name="t_e_year">';
	populate_year();
	
	echo	'</select></td><td width="100px">&nbsp;</td>
		  </tr>
		  <tr>
		  <td>Survey Begins:</td>
		  <td><p><select name="s_s_month">';
		  
	populate_month();
	
	echo		  '</select>
			  &nbsp;
			  <input type="text" maxlength="2" size="1" name="s_s_day" onblur="confirmSubmit(document.forms[0].s_s_month.value, document.forms[0].s_s_day.value, document.forms[0].s_s_year.value, document.forms[0].s_s_day.name);" disabled="true">
			  , <select name="s_s_year">';
	populate_year();
	
	echo	'</select></td><td>&nbsp;</td>
		  </tr>
		  <tr>
		  <td>Survey Ends:</td>
		  <td><p><select name="s_e_month">';
		  
	populate_month();
	
	echo '</select>
			  &nbsp;
			  <input type="text" maxlength="2" size="1" name="s_e_day" onblur="confirmSubmit(document.forms[0].s_e_month.value, document.forms[0].s_e_day.value, document.forms[0].s_e_year.value, document.forms[0].s_e_day.name);" disabled="true">
			  , <select name="s_e_year">';
	populate_year();
	
	echo	'</select></td><td>&nbsp;</td>
		  </tr>
		  <tr>
			  <td>Survey Link<br>(if changed):</td>
			  <td><input type="text" maxlength="256" size="30" name="link"></td>
		  </tr>
		  <tr>
		  <td><input type="submit" onclick="confirmSubmit();" name="SubmitForm" value="Send" disabled="disabled"></td>
		  </tr>
		  </table>
		  </form>
		  </div>';
		  
	echo '<div class="feedback">'.$feedback.'</div>';
	
	$current_year = date("Y");
	
	echo '</td><td rowspan="2"><div>
		  <p class="Titles">'.$current_year.' Survey Information</p>
		  <table border="1" class="text" cellpadding="2" cellspacing="1">
		  <tr><th>Term</th><th>Survey Start Date</th><th>Survey Close Date</th></tr>';
		  
	$query = 'SELECT * FROM mdl_term_info WHERE termid LIKE "'.$current_year.'%"';
	if (date("F") == 'December'){
		$query = $query.' OR termid LIKE "'.($current_year + 1).'%"';
	}
	$query = $query.' ORDER BY termid';
	$result = query_function($query);
	if (count_results($result) == 0){
		echo '<tr><td colspan="3" align="center">No Information To Display.</td></tr>';
	}
	$counter = 1;
	$now = 1;
	$today = mktime(0, 0, 0, date('m'), date('d'), date('Y')); 
	while(($row = fetch_assc($result)) && ($counter <= 12)) {
		
		$st = $row['startdate'];
		$cl = $row['closedate'];
		
		echo '<tr';
		if ($today > $cl) {
			echo ' bgcolor="#CCCCCC"';
		}
		
		if ($today >= $st && $today <= $cl && ($now == 1)) {
			echo ' bgcolor="#FFFF9C" style="font-weight:bold;"';
			$now = 0;
		}
		
		echo '><td>'.$row['termid'].'</td>
				<td align="right">'.date('F\ jS\, Y', $st).'</td>';
	
		echo   '<td align="right">'.date('F\ jS\, Y', $cl).'</td>
				</tr>';
		$counter++;
	} 
		
	$query = 'SELECT link FROM mdl_term_info WHERE link <> " " AND startdate <= "'.$today.'" ORDER BY termid';
	$result = query_function($query);
	$count = count($result);
	while ($row = fetch_assc($result)) {
		$current_link = $row['link'];
	}
	
	if (isset($current_link)){
		//validate link
		$pos = strpos($current_link, 'http://');
		if ($pos === false) {
			$current_link = 'http://'.$current_link; 
		}
				
	}

	echo '</table>
		  <p><strong>Current survey link:</strong><br />';
	if (!isset($current_link)) {
		echo ' <strong style="color:red;">NO SURVEY LINK PROVIDED</strong>';
	} else {
		echo  '<a href="'.$current_link.'" target="_blank"> '.$current_link.'</a>';
	}
	echo '</p>
		  <p>&nbsp;</p>
		  <form action="term_delete.php" method="POST">
		  <p>Delete Entry: <select name="remove_term">';
		  
	$d_query = 'SELECT termid FROM mdl_term_info WHERE termid LIKE "'.date('Y').'%" ORDER BY termid';
	$d_results = mysql_query($d_query);
	if (count_results($d_results) != 0) {
		while ($row = mysql_fetch_assoc($d_results)){
			echo '<option value="'.$row['termid'].'">'.$row['termid'].'</option>';
		}
			
		echo '</select>
			  <input type="submit" value="Delete Entry">';
	} else {
		echo '<option value="">No Records</option>
			  <input type="submit" value="Delete Entry" disabled="disabled">';
	}
	echo  '</p></form></td></div></tr>
		  <tr height="188px"><td>&nbsp;</td></tr></table>
		  </div>
		  <div id="no_validate" class="error"><h1 class="important">This page cannot be shown unless Javascript is enabled in your browser. <br />Please change this setting and refresh to see this page.</h1></div>
		  </div>
		  </body>
		  </html>';

?>