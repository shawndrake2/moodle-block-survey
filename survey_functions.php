<?php

//Code by Shawn Drake - June 2009

function populate_month() {
	
	$counter = 1;
	$month = date('n');
	while ($counter <= 12) {
		echo '<option value="'.$month.'">'.date( "F" , mktime( 0 , 0 , 0 , $month, 1 ) ).'</option>';
		$counter++;
		if ($month == 12) {
			$month = 1;
		} else {
			$month = $month + 1;
		}
	}
	
}

function populate_year() {
	
	$counter = 1;
	$year = (date('Y') - 1);
	while ($counter <=3) {
		echo '<option value="'.$year;
		if ($year == date('Y')) {
			echo '" selected="selected';
		}
		echo '">'.$year.'</option>';
		$counter++;
		$year = $year + 1;
	}
}

function validate_entries($ts_month, $ts_day, $ts_year, $te_month, $te_day, $te_year, $ss_month, $ss_day, $ss_year, $se_month, $se_day, $se_year) {
	
	$total_days = cal_days_in_month(CAL_GREGORIAN, $ts_month, $ts_year);
	if (isset($ts_day) && $ts_day >0 && $ts_day <= $total_days){
		$total_days = cal_days_in_month(CAL_GREGORIAN, $te_month, $te_year);
		if (isset($te_day) && $te_day >0 && $te_day <= $total_days){
			$total_days = cal_days_in_month(CAL_GREGORIAN, $ss_month, $ss_year);
			if (isset($ss_day) && $ss_day >0 && $ss_day <= $total_days){
				$total_days = cal_days_in_month(CAL_GREGORIAN, $se_month, $se_year);
				if (isset($se_day) && $se_day >0 && $se_day <= $total_days){
					
					return true;
				}
			}
		}
	} else {
		return false;
	}	
}

?>