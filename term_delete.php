<?php

require_once ('survey_config.php');
require_once ('db.php');

$delete = 0;
if (isset($_POST['remove_term'])) {
	$term = $_POST['remove_term'];
} else {
	$term = $_POST['delete'];
}

echo	'<style type="text/css">

		.important {
			color: #F00;
			font-weight: bold;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 12pt;
		}
		
		.error {
			width: 300px;
			height: 300px;
			background-color:#FFF;
			border-color:#FF0;
			text-align:center;
			margin:auto;
		}
		
		.container {
			width: 100%;
			height: 100%;
			text-align: center;
			margin: auto;
		</style>
		
		<html>
		<head><title>Delete Term Information</title></head>
		<body width="100%"><div class="container">';
		
if (isset($_POST['delete'])) {
	$delete = 1;
}

if ($delete == 1) {
	
	//connect to the database
	db_connection();

	$query = 'DELETE FROM mdl_term_info WHERE termid = "'.$term.'"';
	$execute = query_function($query);
	
	echo '<div class="error"><h1 class="important">Information for term '.$term.' has been deleted.</h1><br />
		 <input type="button" onclick="Javascript:parent.location=\'survey_info.php\'" value="OK"></div>';
} else {

	echo 	'<form action="term_delete.php" method="POST">
			<div class="error"><h1 class="important">Are you sure you want to delete information for term '.$term.'?</h1><br />
			<input type="hidden" name="delete" value="'.$term.'">
			<input type="submit" value="Delete Now"><input type="button" onclick="Javascript:parent.location=\'survey_info.php\'" value="Cancel"></div>
			</form>';
			
}

echo '</div></body>
	 </html>';

?>